variable "region" {
  description = "Name to be used on all resources as prefix"
  type        = string
  default     = "us-east-1"
}